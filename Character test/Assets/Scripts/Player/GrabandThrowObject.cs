﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabandThrowObject : MonoBehaviour
{

    public Transform player;
    public Transform weaponHolder;
    public float throwForce = 10;
    public bool hasPlayer = false;
    public bool beingCarried = false;
    public int dmg;
    private bool touched = false;

    void Start()
    {

    }

    void FixedUpdate()
    {
        if (player)
        {
            weaponHolder = player.GetChild(0);
            hasPlayer = true;
            GetComponent<Rigidbody>().isKinematic = true;
            this.transform.position = weaponHolder.transform.position;
            this.transform.rotation = weaponHolder.transform.rotation;
            this.transform.parent = weaponHolder.transform;
            beingCarried = true;
        }
        else
        {
            hasPlayer = false;
            weaponHolder = null;
        }

        if (beingCarried)
        {
            if (touched)
            {
                player = null;
                GetComponent<Rigidbody>().isKinematic = false;
                this.transform.parent = null;
                beingCarried = false;
                touched = false;
            }
            if (Input.GetMouseButtonDown(0))
            {
                player = null;
                GetComponent<Rigidbody>().isKinematic = false;
                this.transform.parent = null;
                beingCarried = false;
                GetComponent<Rigidbody>().AddForce(weaponHolder.forward * throwForce);
            }
            else if (Input.GetMouseButtonDown(1))
            {
                player = null;
                GetComponent<Rigidbody>().isKinematic = false;
                this.transform.position = weaponHolder.transform.position;
                this.transform.parent = null;
                beingCarried = false;
            }
        }
    }

    void OnTriggerEnter()
    {
        if (beingCarried)
        {
            touched = true;
        }
    }
}
