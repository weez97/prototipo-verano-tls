﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{

    Vector3 velocity;
    Rigidbody rb;

    public float jumpForce;

    public bool isAirborne;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }


    public void Move(Vector3 _velocity)
    {
        velocity = _velocity;
    }

    public void LookAt(Vector3 lookPoint)
    {
        Vector3 heightCorrectedPoint = new Vector3(lookPoint.x, transform.position.y, lookPoint.z);
        transform.LookAt(heightCorrectedPoint);
    }

    void Update()
    {
        GrabObjects();
        CheckFloor();
        if (Input.GetKeyDown(KeyCode.Space))
            rb.AddForce(transform.up * jumpForce, ForceMode.Impulse);
    }

    public void FixedUpdate()
    {
        rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);

    }

    public void GrabObjects()
    {
        if (Input.GetButtonDown("Use"))
        {
            RaycastHit hit;
            // Debug.DrawRay(new Vector3(transform.GetChild(0).position.x, 0, transform.GetChild(0).position.z), transform.forward, Color.black, 0);
            if (Physics.Raycast(new Vector3(transform.GetChild(0).position.x, 0, transform.GetChild(0).position.z), transform.forward, out hit, 2.5f))
            {
                if (hit.collider.CompareTag("PickableObject"))
                {
                    hit.transform.GetComponent<GrabandThrowObject>().player = this.transform;
                }
            }
        }
    }

    public void CheckFloor()
    {
        RaycastHit hit;
        // Debug.DrawRay(transform.position, transform.up * -1, Color.red, 1);
        if (Physics.Raycast(transform.position, transform.up * -1, out hit, 2f))
        {
            // Debug.Log(hit.collider.tag);
            if (hit.collider.CompareTag("Floor"))
            {

                isAirborne = false;
                return;
            }

        }
        else
        {
            if (!isAirborne)
            {
                rb.AddForce(transform.up * jumpForce, ForceMode.Impulse);
                isAirborne = true;
                Debug.Log("salto");
            }
            else return;
        }
    }
}
